
from api.core import logger, create_response, serialize_list
from api.models import db, Game, GameBoard, Player
from .tictactoe_controller import TicTacToe


class GameController:

    @classmethod
    def validate_creation(cls, data):
        """
        Validates the user input and returns in case it's wrong
        """
        if "players" not in data:
            msg = "Missing players for creation of the game."
            logger.info(msg)
            return create_response(status=422, message=msg)
        if "starting_player" not in data:
            msg = "Missing information of the game."
            logger.info(msg)
            return create_response(status=422, message=msg)
        return cls._creation(data)
        # Create SQL ALCHEMY objects

    @classmethod
    def _create_players_and_id(cls, players, game_id, first):
        """
        Creates players of the game in the DB
        """
        for player in players:
            Flag = 1 if player["name"] == first else 0
            new_player = Player(
                player_name=player["name"],
                player_tag=player["player_mark"],
                player_game_id=game_id,
                turn_to_play=Flag
            )
            db.session.add(new_player)
            db.session.commit()
        return players

    @classmethod
    def _creation(cls, game_data):
        """
        Creates the Game model and the Game Board Model
        """
        new_game = Game(ongoing_player=game_data["starting_player"])
        new_board = GameBoard()
        db.session.add_all([new_game, new_board])
        # commit it to database
        db.session.commit()
        players = cls._create_players_and_id(
            game_data["players"], new_game.id_game, game_data["starting_player"])
        return create_response(
            message=f"Successfully created Game with ID: {new_game.id_game} "
                    f"Players in Game: {players} "
                    f"State of the board: {new_game.gamemovements} "
        )

    @classmethod
    def get_game_list(cls, args):
        """
        Gets all the games present in the DB
        """
        game = Game.find_all(args)
        return create_response(
            message="List of Games:",
            data={"Games": serialize_list(game)}
        )

    @classmethod
    def get_game(cls, game_id):
        """
        Gets a Specific Game from the DB
        """
        game = Game.find_one(game_id)
        return create_response(
            message="The game you asked for",
            data={"Games": serialize_list(game)}
        )


    @staticmethod
    def delete_game(game_id):
        """
        Deletes the game on the DB // No soft Delete
        """
        game = Game.delete_one(game_id)
        return game

    @staticmethod
    def submit_play(game_data):
        """
        Submits the play to the tictactoe controller
        """
        move = TicTacToe.start_move(game_data)
        return move