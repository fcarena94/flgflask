
from api.core import create_response
from api.models import db
from api.models.game import Game, GameBoard, Player


class TicTacToe:

    @classmethod
    def get_coordinates(cls, x, y):
        """
        Verifies that the coordinates, are inside the board
        """
        if 1 > x or x > 3 or 1 > y or y > 3:
            return create_response(
                status=422,
                message=f"Bad Coordinates"
            )
        return x, y

    @classmethod
    def validate_turn(cls, user, id):
        """
        Simple validation of object in query to check if is the turn of the player
        """
        obj = Game.find_user(user, id)
        if obj is not None:
            return create_response(
                status=422,
                message=f"Not your Turn"
            )

    @classmethod
    def coordinates_empty(cls, x, y, board):
        """Check to see if coordinates of board are empty"""
        if board.game_board[y - 1][x - 1] != ' ':
            return create_response(
                status=422,
                message=f"Space Taken"
            )
        else:
            return (x, y)

    @classmethod
    def find_players(cls, game_id):
        """
        Find players of the game in the db and switch the playing user
        """
        bplayer = Player.find_next_to_play(game_id)
        aplayer = Player.find_playing(game_id)
        bplayer.turn_to_play = True
        aplayer.turn_to_play = False
        db.session.commit()
        return aplayer, bplayer

    @classmethod
    def save_movement(cls, row, column, game_id, player):
        """
        Get the board from the model, asssign the value that
        the player choose, and updates the model
        """
        board = GameBoard.find_one(game_id)
        new_board = board
        cls.coordinates_empty(row, column, board)
        row = row - 1
        column = column -1
        new_board.game_board[row][column] = player.player_tag
        board.game_board = new_board.game_board
        #TODO Fix problem with postgress and JSON type...
        db.session.commit
        return board

    @classmethod
    def start_move(cls, data):
        """
        Cascade of functions to make the move, and return the information to user
        """
        row = data["row"]
        column = data["column"]
        player = data["player"]
        game_id = data["game_id"]
        cls.get_coordinates(row , column)
        cls.validate_turn(player, game_id)
        player, playerb = cls.find_players(game_id)
        movement = cls.save_movement(row, column, game_id, player)
        return create_response(
                status=200,
                message=f"Movement Made, details:"
                        f" Player A: {player.player_name, player.player_tag} \n"
                        f" Player B: {playerb.player_name, playerb.player_tag} \n"
                        f" New board: {movement.game_board}"
                        f" Game Info: {game_id}"
        )