from api.core import Mixin, logger
from .base import db

from sqlalchemy import ForeignKey
from psycopg2 import extras


class Player(db.Model):
    """
    Player Model
    """
    player_id = db.Column(db.Integer, unique=True, primary_key=True)
    player_name = db.Column(db.String, nullable=False)
    player_tag = db.Column(db.String, nullable=False)
    player_game_id = db.Column(db.Integer, nullable=True)
    turn_to_play = db.Column(db.Boolean, nullable=False)

    @classmethod
    def find_playing(cls, game_id):
        obj = db.session.query(Player).filter_by(player_game_id=game_id,
                                                    turn_to_play=True).first()
        logger.info(obj)
        return obj

    @classmethod
    def find_next_to_play(cls, game_id):
        obj = db.session.query(Player).filter_by(player_game_id=game_id,
                                                    turn_to_play=False).first()
        logger.info(obj)
        return obj


class Game(Mixin, db.Model):
    """Table to storage Game information"""
    __tablename__ = "games"

    id_game = db.Column(db.Integer, unique=True, primary_key=True)
    ongoing_player = db.Column(db.String, nullable=False)
    game_going = db.Column(db.Boolean, nullable=False, default=True)
    game_winner = db.Column(db.String, nullable=True)
    game_winner_id = db.Column(db.Integer, nullable=True)
    #Relationship
    gamemovements = db.relationship("GameBoard", back_populates="games")

    @classmethod
    def find_all(cls, filter):
        if not filter:
            return Game.query.all()
        else:
            return Game.query.filter_by(game_going=filter)

    @classmethod
    def find_one(cls, game_id):
        filter = game_id["game_id"]
        return Game.query.filter_by(id_game=filter)

    @classmethod
    def delete_one(cls, game_id):
        filter = game_id["game_id"]
        obj = Game.query.filter_by(game_going=filter)
        db.session.delete(obj)
        db.session.commit()
        return obj

    @classmethod
    def find_user(cls, user, game_id):
        find = Game.query.filter_by(id_game=game_id, ongoing_player=user).first()
        return find


class GameBoard(Mixin, db.Model):
    """GameBoard table, used to storage all the movements of a game"""

    __tablename__ = "gamemovements"

    default_board = [["_" for _ in range(3)] for _ in range(3)]

    id_board = db.Column(db.Integer, unique=True, primary_key=True)
    game_id = db.Column(db.Integer, ForeignKey('games.id_game'))
    game_board = db.Column(db.JSON, default=default_board)
    #Relationship
    games = db.relationship("Game", back_populates="gamemovements")

    @classmethod
    def find_one(cls, game_id):
        obj = db.session.query(GameBoard).filter_by(id_board=game_id).first()
        return obj

