from flask import Blueprint, request
from api.gamehandlers.game_controller import GameController

main = Blueprint("main", __name__)  # initialize blueprint


@main.route("/creategame", methods=["POST"])
def create_game():
    data = request.get_json()
    game = GameController.validate_creation(data)
    return game


@main.route("/submitplay", methods=["POST"])
def submit_play():
    data = request.get_json()
    play = GameController.submit_play(data)
    return play


@main.route("/getgames", methods=["POST"])
def get_game():
    data = request.get_json()
    game = GameController.get_game(data)
    return game


@main.route("/listgames", methods=["GET"])
def list_game():
    args = request.args.get("playing")
    return GameController.get_game_list(args)


@main.route("/deletegame", methods=["POST"])
def delete_game():
    data = request.get_json()
    game = GameController.delete_game(data)
    return game


