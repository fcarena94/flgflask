import pytest
from api.gamehandlers.tictactoe_controller import TicTacToe


def test_doordinates_ok():
    """
    Validate coordinates
    """
    x = 2
    y = 3
    result = TicTacToe.get_coordinates(x,y)
    assert result[0] == x
    assert result[1] == y
