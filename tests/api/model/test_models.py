from api.models import db
from api.models.game import Player, Game, GameBoard


def test_new_player():
    """
    Check that the Player model populates OK
    """
    player = Player(
        player_name="Foo Bar",
        player_tag="Y",
        player_game_id="1234",
        turn_to_play=True
    )
    assert player.player_name == 'Foo Bar'
    assert player.player_tag == 'Y'
    assert player.player_game_id == '1234'
    assert player.turn_to_play == True

def test_new_game():
    """
    Check that Games model populates OK
    """
    game = Game(
        ongoing_player="Bar Foo",
        game_going=False,
        game_winner="Foo Bar",
        game_winner_id=8923
    )
    assert game.ongoing_player == "Bar Foo"
    assert game.game_going == False
    assert game.game_winner == "Foo Bar"
    assert game.game_winner_id== 8923

def test_new_board():
    """
    Check that Game Board model Populates OK
    """

    game_board = GameBoard(
        id_board = 1,
        game_id = 2
    )
    assert game_board.id_board == 1
    assert game_board.game_id == 2