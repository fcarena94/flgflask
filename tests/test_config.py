import os


def test_development_config(client):
    client.config.from_object('project.config.DevelopmentConfig')
    assert client.config['DEBUG']
    assert not client.config['TESTING']
    assert client.config['SQLALCHEMY_DATABASE_URI'] == os.environ.get(
        'DATABASE_URL')


def test_testing_config(client):
    client.config.from_object('project.config.TestingConfig')
    assert client.config['DEBUG']
    assert client.config['TESTING']
    assert not client.config['PRESERVE_CONTEXT_ON_EXCEPTION']
    assert client.config['SQLALCHEMY_DATABASE_URI'] == os.environ.get(
        'DATABASE_TEST_URL')


def test_production_config(client):
    client.config.from_object('project.config.ProductionConfig')
    assert not client.config['DEBUG']
    assert not client.config['TESTING']
    assert client.config['SQLALCHEMY_DATABASE_URI'] == os.environ.get(
        'DATABASE_URL')